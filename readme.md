Welcome to Airdoper contract development
========================================

Spread ERC20 and ERC223 tokens to the world, cheap, fast and trustless.

Usage
=====

Current address: `0xf52c0604b2E2F321033ba8b7B7dcc7B07691089e`
Ropsten (testnet) current address: `0xfd45522921cf350b6531b972232ecb4bbdc54865`

Airdroper is smart contract can make multiple token transaction at a time.

**Deposit**

First of all, you need to deposit spreader, there is multiple methods to do this:

1. Using `approve()` & `transferFrom()` (old as ERC20)
2. Using direct `transfer()` (new as ERC223)
3. Deposit and spread using `transfer()` with `data` (coolest)

**Deposit using `approve()` & `transferFrom()`**

If you spread old ERC20 tokens, you need to use this method.

It's ancient method from ERC20 age. During this time, there is no way to make direct `transfer()` to contract address, if you accidentally done this, transfered token is stuck in a contract address forever, FOR...EVER! It's really bad. Only way to do this, `approve()` and `transferFrom()` methods, bit it needs two serial transaction, it's bad too, cuz you need more time and cost. In other words, please avoid to use it, it's very non-recommended and only for backward compatiblity for ERC20 tokens.

If you have ERC20 token to spread, OK, let me know:

1. Approve using `approve()` of token's contract to this spreader contract to spend. Use this like `spread("[SPREADER_CONTRACT_ADDRESS]", 100)`, 100 is amount of token to approve.
2. Deposit using `deposit()` method of this spreader contract, it used `transferFrom()` of token contract to make transfer tokens to spreader contract and records who deposites how mutch token and what kind of token. Use this like `deposit([TOKEN_ADDRESS], 100)`, second argument is amount of token, and it means it must be equal or smaller than approved ammount.

**Deposit using `transfer`**

If you have ERC223 token and directly transfers to this contract address, it's automatically deposited. BUT, YOU DON'T IF YOU HAVE ERC20 TOKENS, PLEASE READ ABOVE!

**Deposit and spread using `transfer()` with `data`**

If you have ERC223 token and want to do super duper cooler thing, OK, just do it! BUT, PLEASE, PLEASE, PLEADE DON'T IF YOU HAVE ERC20 TOKENS, READ ABOVE!

It's all in one method. Deposit and spread, they done in one transaction, it saves your money and time. You need to generate `spread()` function caller in `bytes` type and make `transaction()` with this data.

Folowing lines of code using web3.js is helps generate caller data:

```javascript
var addresses = [
    '0x0000000000000000000000000000000000000001',
    '0x0000000000000000000000000000000000000002'
]

var amounts = [
    1,
    1
]

var function_sig = web3.eth.abi.encodeFunctionSignature('spread(address,address[],uint256[])')

var parameters = web3.eth.abi.encodeParameters(['address','address[]', 'uint256[]'], [config.token_address, addresses, amounts])

console.log(sigofun + parameters.replace('0x', ''))
```
Call `transfer()` of token with following arguments, first is token address, second is amount of token to deposit and last is data generated above. It looks like `transfer("0x123...xyz", 100, "0x321...abc")`.

**Spread**

Now is't time to make it rain. Call `spread([TOKEN_ADDRESS], [ADDRESSES_AS_ARRAY], [AMOUNTS_AS_ARRAY])`, second and third arguments depends on order of p, ex. `spread("0x123...abc", ["0x321...abc", "0x321...bca", ...], [1000, 2000, ...])`.

**Check balance**

Using `balanceOf()` with arguments that token address and user address like `balanceOf([TOKEN_ADDRESS], [USER_ADDRESS])`.

Development
===========

You must use truffle

```
sudo npm install truffle -g
```

**TODO**

- [ ] Write test
- [x] Just do it!

Deployment
==========

1. Develop smart contract(s) on `./contracts` folder

2. Compile

```
truffle compile
```

3. Create new migration file

```
cp ./migrations/[n]_deploy_contracts.js ./migrations/[n+1]_deploy_contracts.js
```
`[n]` must be current (largest) number in `./migrations` folder.

4. Migrate

```
truffle migrate --network [NETWORK]
```
`[NETWORK]` must be one of `development` or something as what you configured in `./truffle.js` file.
